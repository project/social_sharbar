INTRODUCTION
------------
 
This module let you add social sharebar on your website. 
By default you will find following sharebar.

Linkedin
Google+
Twitter
Facebook
Email.

REQUIREMENTS 
------------

 - filter
 - token
 
INSTALLATION
------------

Install as usual, see
https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 
for further information.

CONFIGURATION
------------

After installation navigate to admin/config/sharebar/sharebarsettings
Configure settings as per your requirements.
Here you can even add more sharebar button..

Now navigate to admin/structure/block
And Make sure sharebar block is available in content area.


MAINTAINERS
-----------

Current maintainers:

 * Md Meraj Ahmed (https://www.drupal.org/user/3503902/)
